package br.com.caelum.argentum.indicadores;

import br.com.caelum.argentum.modelo.Candle;
import br.com.caelum.argentum.modelo.SerieTemporal;

public class MediaMovelPonderada implements Indicador {
		
	private Indicador outroIndicador;

	public MediaMovelPonderada(Indicador outroIndicador) {
		this.setOutroIndicador(outroIndicador);
	}

	public double calcula(int posicao, SerieTemporal serie) {
		double soma = 0.0;
		int peso = 3;

		for (int i = posicao; i > posicao - 3; i--) {

			this.outroIndicador.calcula(i, serie);
			
			//this.outroIndicador = serie.getCandle(i);
			soma += this.outroIndicador.calcula(i, serie) * peso;
			peso--;
		}
		
		return soma / 6;
	}

	public String toString() {
		return "MMP de "+ this.outroIndicador.toString();
	}

	public Indicador getOutroIndicador() {
		return outroIndicador;
	}

	public void setOutroIndicador(Indicador outroIndicador) {
		this.outroIndicador = outroIndicador;
	}

}
