package br.com.caelum.argentum.modelo;

import java.util.Calendar;

public final class Candle {

	private final double abertura;
	private final double fechamento;
	private final double minimo;
	private final double maximo;
	private final double volume;
	private final Calendar data;

	public Candle(double abertura, double fechamento, double minimo, double maximo, double volume, Calendar data) {

		if (maximo < minimo || data == null) {
			throw new IllegalArgumentException("O valor m�ximo n�o pode ser menor que o m�nimo");
		}

		if (abertura < 0 || fechamento < 0 || minimo < 0 || maximo < 0 || volume < 0) {
			throw new IllegalArgumentException("O valor m�ximo n�o pode ser menor que o m�nimo");
		}

		this.abertura = abertura;
		this.fechamento = fechamento;
		this.minimo = minimo;
		this.maximo = maximo;
		this.volume = volume;
		this.data = data;
	}

	public double getAbertura() {
		return abertura;
	}

	public double getFechamento() {
		return fechamento;
	}

	public double getMinimo() {
		return minimo;
	}

	public double getMaximo() {
		return maximo;
	}

	public double getVolume() {
		return volume;
	}

	public Calendar getData() {
		return data;
	}

	public boolean isAlta() {
		return this.abertura <= this.fechamento;
	}

	public boolean isBaixa() {
		return this.abertura > this.fechamento;
	}

	@Override
	public String toString() {
		return "[Abertura " + this.abertura + ", Fechamento " + this.fechamento + ", M�nima " + this.minimo
				+ ", M�xima " + this.maximo + ", Volume " + volume + ", Data " + data + "]";
	}

}
