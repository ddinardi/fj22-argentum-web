package br.com.caelum.argentum.modelo;

import java.util.Calendar;

public class CandleBuilder {

	private double abertura;
	private double fechamento;
	private double minimo;
	private double maximo;
	private double volume;
	private Calendar data;
	
	@SuppressWarnings("unused")
	private boolean testes[];

	public CandleBuilder() {
		this.testes = new boolean [6];
	}

	public CandleBuilder comAbertura(double abertura) {
		this.abertura = abertura;
		this.testes[0] = true;
		return this;
	}

	public CandleBuilder comFechamento(double fechamento) {
		this.fechamento = fechamento;
		this.testes[1] = true;
		return this;
	}

	public CandleBuilder comMinimo(double minimo) {
		this.minimo = minimo;
		this.testes[2] = true;
		return this;
	}

	public CandleBuilder comMaximo(double maximo) {
		this.maximo = maximo;
		this.testes[3] = true;
		return this;
	}

	public CandleBuilder comVolume(double volume) {
		this.volume = volume;
		this.testes[4] = true;
		return this;
	}

	public CandleBuilder comData(Calendar data) {
		this.data = data;
		this.testes[5] = true;
		return this;
	}

	public Candle geraCandle() {
		for (int i = 0; i < testes.length; i++) {
			if (testes[i] == false)
			{
				throw new IllegalStateException();
			}
		}
		return new Candle(abertura, fechamento, minimo, maximo, volume, data);
	}

}
