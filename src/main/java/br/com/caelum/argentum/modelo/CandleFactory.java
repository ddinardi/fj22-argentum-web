package br.com.caelum.argentum.modelo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CandleFactory {

	public Candle constroiCandleParaData(Calendar data, List<Negociacao> negociacoes) {
		double maximo = negociacoes.isEmpty() ? 0 : negociacoes.get(0).getPreco();

		double minimo = negociacoes.isEmpty() ? 0 : negociacoes.get(0).getPreco();

		double volume = 0;
		// digite foreach e d� um ctrl + espa�o para ajudar a
		// criar o bloco
		// abaixo!
		for (Negociacao negociacao : negociacoes) {
			volume += negociacao.getVolume();

			if (negociacao.getPreco() > maximo) {
				maximo = negociacao.getPreco();
			} else if (negociacao.getPreco() < minimo) {
				minimo = negociacao.getPreco();
			}
		}

		double abertura = negociacoes.isEmpty() ? 0 : negociacoes.get(0).getPreco();
		double fechamento = negociacoes.isEmpty() ? 0 : negociacoes.get(negociacoes.size() - 1).getPreco();

		return new Candle(abertura, fechamento, minimo, maximo, volume, data);
	}

	/*
	 * public List<Candle> constroiCandles(List<Negociacao> negociacoes) {
	 * ArrayList<Candle> candles = new ArrayList<Candle>(); Calendar data =
	 * null; ArrayList<Negociacao> negociacoesParaCandle = new
	 * ArrayList<Negociacao>(); if (candles.isEmpty()) { data =
	 * negociacoes.get(0).getData(); } for (Negociacao negociacao : negociacoes)
	 * {
	 * 
	 * if (negociacao.getData().before(data)) { throw new IllegalStateException(
	 * "negocia��es em ordem errada"); } if (negociacoesParaCandle.isEmpty()) {
	 * negociacoesParaCandle.add(negociacao); data = negociacao.getData(); }
	 * else {
	 * 
	 * if
	 * (negociacao.isMesmoDia(negociacoesParaCandle.get(negociacoesParaCandle.
	 * size() - 1).getData())) { negociacoesParaCandle.add(negociacao); data =
	 * negociacao.getData();
	 * 
	 * if (negociacao.equals(negociacoes.get(negociacoes.size() - 1))) { Candle
	 * candle_Final = constroiCandleParaData(
	 * negociacoesParaCandle.get(negociacoesParaCandle.size() - 1).getData(),
	 * negociacoesParaCandle); candles.add(candle_Final); } } else { Candle
	 * candle_Final = constroiCandleParaData(
	 * negociacoesParaCandle.get(negociacoesParaCandle.size() - 1).getData(),
	 * negociacoesParaCandle); candles.add(candle_Final);
	 * negociacoesParaCandle.clear(); negociacoesParaCandle.add(negociacao); } }
	 * }
	 * 
	 * return candles; }
	 * 
	 */

	public List<Candle> constroiCandles(List<Negociacao> todasNegociacoes) {
		List<Candle> candles = new ArrayList<Candle>();
		List<Negociacao> negociacoesDoDia = new ArrayList<Negociacao>();
		Calendar dataAtual = todasNegociacoes.get(0).getData();

		for (Negociacao negociacao : todasNegociacoes) {
			if (negociacao.getData().before(dataAtual)) {
				throw new IllegalStateException("negocia��es em ordem errada");
			}
			// se n�o for mesmo dia, fecha candle e reinicia vari�veis
			if (!negociacao.isMesmoDia(dataAtual)) {
				Candle candleDoDia = constroiCandleParaData(dataAtual, negociacoesDoDia);
				candles.add(candleDoDia);
				negociacoesDoDia = new ArrayList<Negociacao>();
				dataAtual = negociacao.getData();
			}
			negociacoesDoDia.add(negociacao);
		} // adiciona �ltimo candle
		Candle candleDoDia = constroiCandleParaData(dataAtual, negociacoesDoDia);
		candles.add(candleDoDia);
		return candles;
	}
}