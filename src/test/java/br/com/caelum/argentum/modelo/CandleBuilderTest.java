package br.com.caelum.argentum.modelo;

import static org.junit.Assert.*;

import java.util.GregorianCalendar;

import org.junit.Test;

public class CandleBuilderTest {

	@Test (expected = IllegalStateException.class)
	public void geracaoDeCandleDeveTerTodosOsDadosNecessarios() {
		CandleBuilder c = new CandleBuilder();
		CandleBuilder builder = new CandleBuilder();
		builder.comAbertura(40.5);
		builder.comFechamento(42.3);
		builder.comMinimo(39.8);
		builder.comMaximo(45.0);
		//builder.comVolume(145234.20);
		builder.comData(new GregorianCalendar(2012, 8, 12, 0, 0, 0));
		Candle candle = builder.geraCandle();
	}

}
