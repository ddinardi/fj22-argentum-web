package br.com.caelum.argentum.testes;

import java.util.GregorianCalendar;

import br.com.caelum.argentum.modelo.CandleBuilder;
import br.com.caelum.argentum.modelo.Candle;

public class TestaCandlestickFactory {

	public static void main(String[] args) {
		/*
		 * Calendar hoje = Calendar.getInstance();
		 * 
		 * Negociacao negociacao1 = new Negociacao(40.5, 100, hoje); Negociacao
		 * negociacao2 = new Negociacao(45.0, 100, hoje); Negociacao negociacao3
		 * = new Negociacao(39.8, 100, hoje); Negociacao negociacao4 = new
		 * Negociacao(42.3, 100, hoje);
		 * 
		 * List<Negociacao> negociacoes = Arrays.asList(negociacao1,
		 * negociacao2, negociacao3, negociacao4);
		 * 
		 * CandlestickFactory fabrica = new CandlestickFactory();
		 * 
		 * Candlestick candle = fabrica.constroiCandleParaData( hoje,
		 * negociacoes);
		 */

		CandleBuilder builder = new CandleBuilder();
		builder.comAbertura(40.5);
		builder.comFechamento(42.3);
		builder.comMinimo(39.8);
		builder.comMaximo(45.0);
		builder.comVolume(145234.20);
		builder.comData(new GregorianCalendar(2012, 8, 12, 0, 0, 0));
		Candle candle = builder.geraCandle();

		System.out.println(candle.getAbertura());
		System.out.println(candle.getFechamento());
		System.out.println(candle.getMinimo());
		System.out.println(candle.getMaximo());
		System.out.println(candle.getVolume());
	}

}
